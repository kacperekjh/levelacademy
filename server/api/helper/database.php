<?php
namespace DataBase;
use Exceptions\ServerErrorCode;
use Exceptions\ServerException;

include_once 'config.php';
include_once 'exceptions.php';

function connect(): \mysqli
{
    static $instance = null;
    if ($instance === null) {
        $instance = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        $instance->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);
        if ($instance->connect_errno) {
            throw new ServerException("Failed to connect to MySQL: " . $instance->connect_error, ServerErrorCode::DatabaseError);
        }
    }
    return $instance;
}

function query(string $query): \mysqli_result
{
    $connection = connect();
    $result = $connection->query($query);
    if (!$result) {
        throw new ServerException('Failed to query the database: ' . mysqli_error($connection), ServerErrorCode::DatabaseError);
    }
    return $result;
}