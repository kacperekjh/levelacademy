<?php
namespace Exceptions;

include_once 'config.php';

set_exception_handler('\Exceptions\handleUserError');

class UserException extends \Exception
{
    public int $httpStatus;
    public UserErrorCode $errorCode;

    public function __construct(string $message, int $httpStatus, UserErrorCode $code, \Throwable $previous = null)
    {
        parent::__construct($message, 1, $previous);
        $this->httpStatus = $httpStatus;
        $this->errorCode = $code;
    }
}

class BadMethodException extends UserException
{
    public function __construct(string $allowedMethods)
    {
        parent::__construct("Invalid HTTP method. Allowed methods: $allowedMethods", 405, UserErrorCode::BadHTTPMethod);
    }
}

class NotFoundException extends UserException
{
    public function __construct(string $resource)
    {
        parent::__construct("$resource doesn't exist.", 404, UserErrorCode::NotFound);
    }
}

enum ServerErrorCode: int
{
    case DatabaseError = 2;
    case TypeError = 3;
}

enum UserErrorCode: int
{
    case Unknown = 0;
    case BadHTTPMethod = 1;
    case InvalidValue = 2;
    case MissingValue = 3;
    case NotFound = 4;
}

class ServerException extends \Exception
{
    public function __construct(string $message, ServerErrorCode $code, \Throwable $previous = null)
    {
        parent::__construct($message, $code->value, $previous);
    }
}

function handleUserError(\Throwable $exception)
{
    if ($exception instanceof UserException) {
        http_response_code($exception->httpStatus);
        echo json_encode([
            'error' => $exception->errorCode->name,
            'message' => $exception->getMessage()
        ]);
        exit(0);
    } else if (SHOW_NONUSER_ERRORS) {
        http_response_code(500);
        if ($exception instanceof ServerException) {
            echo 'A server error has occured: ' . $exception->getMessage();
        } else {
            echo 'An unknown error has occured: ' . $exception->getMessage();
        }
        die($exception->getCode());
    }
}