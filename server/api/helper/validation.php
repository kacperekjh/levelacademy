<?php
namespace Validation;
use Exceptions\ServerException;
use Exceptions\ServerErrorCode;
use Exceptions\UserErrorCode;
use Exceptions\UserException;

include_once 'exceptions.php';

class InvalidValueException extends UserException
{
    public function __construct(string $name, Type | Types | ObjectType $expected)
    {
        $typesString = '';
        if ($expected instanceof Type) {
            $typesString = $expected->name;
        } else if ($expected instanceof Types) {
            $first = true;
            foreach ($expected as $type) {
                if (!$first) {
                    $typesString .= ' | ';
                }
                $first = false;
                $typesString .= $type->name;
            }
            if (!$expected->required) {
                $typesString .= ' (optional)';
            }
        } else {
            $typesString = 'Object';
        }
        parent::__construct("$name has invalid type. Expected type $typesString.", 400, UserErrorCode::InvalidValue);
    }
}

class MissingValueException extends UserException
{
    public function __construct(string $name)
    {
        parent::__construct("Missing $name.", 400, UserErrorCode::MissingValue);
    }
}

enum Type
{
    case Int;
    case Float;
    case String;
    case Bool;
    case Null;
}

class Types extends \ArrayObject
{
    public bool $required = true;

    public function __construct(array $array = [], bool $required = true)
    {
        foreach ($array as $value)
        {
            if (!($value instanceof Type)) {
                throw new ServerException('$value is not Type.', ServerErrorCode::TypeError);
            }
        }
        parent::__construct($array);
        $this->required = $required;
    }
    
    public function offsetSet(mixed $key, mixed $value): void
    {
        if ($value instanceof Type) {
            parent::offsetSet($key, $value);
            return;
        }
        throw new ServerException('$value is not Type.', ServerErrorCode::TypeError);
    }
}

class ObjectType extends \ArrayObject
{
    public function __construct(array $fields = [])
    {
        foreach ($fields as $key => $types) {
            if (!is_string($key)) {
                throw new ServerException('$key is not string.', ServerErrorCode::TypeError);
            }
            if ($types instanceof Type) {
                $fields[$key] = new Types([$types]);
            } else if (!($types instanceof Types) && !($types instanceof ObjectType)) {
                throw new ServerException('$value is not Type, Types or ObjectType.', ServerErrorCode::TypeError);
            }
        }
        parent::__construct($fields);
    }

    public function offsetSet(mixed $key, mixed $value): void
    {
        if (!is_string($key)) {
            throw new ServerException('$key is not a string.', ServerErrorCode::TypeError);
        }
        if ($value instanceof Types) {
            parent::offsetSet($key, $value);
            return;
        }
        else if ($value instanceof Type) {
            parent::offsetSet($key, new Types([$value]));
            return;
        }
        throw new ServerException('$value is not TypeArray or Type.', ServerErrorCode::TypeError);
    }
}

function validateField(mixed &$value, Types | ObjectType $types, string $name): void
{
    if ($types instanceof ObjectType) {
        validateObject($value, $types, $name);
    } else {
        foreach ($types as $type) {
            if ($value === null) {
                if ($type === Type::Null) {
                    return;
                } else {
                    continue;
                }
            }
            switch ($type) {
                case Type::Int:
                    $new = intval($value);
                    if ($new == $value) {
                        $value = $new;
                        return;
                    }
                    break;
                case Type::Float:
                    $new = floatval($value);
                    if ($new == $value) {
                        $value = $new;
                        return;
                    }
                    break;
                case Type::String:
                    if (is_string($value)) {
                        return;
                    } else if ($value instanceof \Stringable) {
                        $value = $value->__toString();
                        return;
                    }
                    break;
                case Type::Bool:
                    if (is_bool($value)) {
                        return;
                    } else if (is_string($value)) {
                        if (preg_match("/true/i", $value)) {
                            $value = true;
                            return;
                        } else if (preg_match("/false/i", $value)) {
                            $value = false;
                        }
                    }
                    break;
                case Type::Null:
                    if (preg_match("/null/i", $value)) {
                        $value = null;
                        return;
                    }
                    break;
            }
        }
        throw new InvalidValueException($name, $types);
    }
}

function validateObject(array &$object, ObjectType $type, string $objectName = 'body'): void
{
    foreach ($type as $key => $field)
    {
        $name = "$objectName.$key";
        if (!array_key_exists($key, $object)) {
            if ($field->required) {
                throw new MissingValueException($name);
            }
            continue;
        }
        validateField($object[$key], $field, $name);
    }
}

function isValid(array &$object, ObjectType $type, string $objectName = 'body'): bool
{
    try {
        validateObject($object, $type, $objectName);
        return true;
    }
    catch (\Throwable $ex) {
        return false;
    }
}