<?php
use Exceptions\BadMethodException;
use Validation\ObjectType;
use Validation\Types;
use Validation\Type;
include_once 'helper/database.php';
include_once 'helper/exceptions.php';
include_once 'helper/validation.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    throw new BadMethodException('GET');
}

const TYPE = new ObjectType([
    'p' => new Types([Type::Int], false),
    'c' => new Types([Type::Int], false)
]);

Validation\validateObject($_GET, TYPE, 'query');

$query = 'SELECT articles_id, title, subtitle, publish_date, image_path FROM articles';
if (isset($_GET['p']) && isset($_GET['c'])) {
    $offset = $_GET['p'] * $_GET['c'];
    $query .= " LIMIT $offset, {$_GET['c']}";
}
$result = DataBase\query($query);
$total = DataBase\query('SELECT COUNT(*) AS total FROM articles')->fetch_row()[0];
$result = [
    'articles' => $result->fetch_all(MYSQLI_ASSOC),
    'total' => $total
];
echo json_encode($result);