<?php
use Exceptions\BadMethodException;
use Exceptions\NotFoundException;
use Validation\ObjectType;
use Validation\Type;
include_once 'helper/database.php';
include_once 'helper/exceptions.php';
include_once 'helper/validation.php';

const TYPE = new ObjectType([
    'id' => Type::Int
]);

Validation\validateObject($_GET, TYPE, 'query');

$article = DataBase\query("SELECT title, subtitle, publish_date, image_path FROM articles WHERE articles_id = {$_GET['id']}");
if ($article->num_rows == 0) {
    throw new NotFoundException("Article #{$_GET['id']}");
}
$contents = DataBase\query("SELECT header, body FROM articles_content WHERE articles_id = {$_GET['id']} ORDER BY number ASC");
$article = $article->fetch_assoc();
$article['contents'] = $contents->fetch_all(MYSQLI_ASSOC);
echo json_encode($article);