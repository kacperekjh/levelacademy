import { derived, get, writable } from "svelte/store";
import { base } from "$app/paths";

type Tail<T extends [...any]> = T extends [infer _, ...infer T] ? T : never;

type ToUnion<T extends [...any]> = T['length'] extends 0
  ? never
  : T['length'] extends 1
  ? T[0]
  : T[0] | ToUnion<Tail<T>>;

type ToObject<T extends [...any], Type> = T['length'] extends 0
  ? never
  : T['length'] extends 1
  ? {[Key in T[0]]: Type}
  : {[Key in T[0]]: Type} & ToObject<Tail<T>, Type>;

type Pages = ['Home', 'Tournaments', 'Articles', 'Article', 'Summer Camp'];

export type Page = ToUnion<Pages>;

export const paths: Readonly<ToObject<Pages, string>> = {
  'Home': base,
  'Tournaments': `${base}/tournaments`,
  'Articles': `${base}/articles`,
  'Article': `${base}/article`,
  'Summer Camp': `${base}/summercamp`
};

export type NamedPage = {
  page: Page,
  alias?: string
};

export const currentPath = writable<NamedPage[]>();
export const currentPage = derived(currentPath, value => value?.[0]?.page);